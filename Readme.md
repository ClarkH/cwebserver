# Introduction
A simple http server, showing the use of unix's network apis.
It can work on linux and macos.

# Usage
server usage
```shell
mkdir build
cd build
cmake ..
make
./webserver 8000
```