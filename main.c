#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>

typedef struct ClientMsg {
    int cfd;
    struct sockaddr_in addr;
} ClientMsg;

char response_ok[] = "HTTP/1.1 200 OK\r\n" \
                     "Content-Type: text/html\r\n" \
                     "\r\n";

char response_er[] = "HTTP/1.1 404 Not Found\r\n" \
                     "Content-Type: text/html\r\n" \
                     "\r\n" \
                     "<HTML><BODY>File not found</BODY></HTML>";

/**
  char *dst[10] = {0}, **tmp = dst;
  char array[100] = "abc,xyz";
  char *src = array;
  int count = 0;
  split_string(src, ",", &tmp, &count);
 */
void split_string(char *src, const char *delim, char ***dst, int *count) {
    char *token;

    int i = 0;
    while ((token = strsep(&src, delim)) != NULL) {
        (*dst)[i] = token;
        i++;
    }
    *count = i;
}

void parse_request_message(const char data[]) {
    int count = 0;

    char *dst[100] = {0}, **tmp_dst = dst;
    char tmp_data[3000] = {0}, *src = tmp_data;
    strncpy(tmp_data, data, 3000);
    
    split_string(src, "\r\n", &tmp_dst, &count);
    for (size_t i = 0; i < count; i++) {
        fprintf(stderr, "%s\n", dst[i]);
    }
}

void *deal_client_fun(void *arg) {
    ClientMsg *p = (ClientMsg *)arg;

    // print client infos
    char ip[16] = {0};
    inet_ntop(AF_INET, &p->addr.sin_addr.s_addr, ip, 16);
    unsigned short port = ntohs(p->addr.sin_port);
    fprintf(stderr, "%s:%hu connected\n", ip, port);

    char request_data[3000] = {0};
    int recv_len = recv(p->cfd, request_data, sizeof(request_data), 0);
    if (recv_len > 0) {
        //parse_request_message((const char*)request_data);

        char method[10] = {0};
        char path[512]  = {0};
        sscanf((const char*)request_data, "%s %s", method, path);
        fprintf(stderr, "method: %s path: %s\n", method, path);
        if ((strlen(path) == 1 && !memcmp(path, "/", 1)) ||
            (strlen(path) == 11 && !memcmp(path, "/index.html", 11))) {
            int fd = open("../html/index.html", O_RDONLY);
            if (fd > 0) {
                send(p->cfd, response_ok, strlen(response_ok), 0);
                while (1) {
                    unsigned char buf[1024];
                    int ret = read(fd, buf, sizeof(buf));
                    if (ret > 0) {
                        send(p->cfd, buf, ret, 0);
                    } else {
                        break;
                    }
                }
                close(fd);
            } else {
                perror("open");
                send(p->cfd, response_er, strlen(response_er), 0);
            }
        } else {
            send(p->cfd, response_er, strlen(response_er), 0);
        }
    } else {
        perror("recv");
    }

    close(p->cfd);
    free(p);
    pthread_exit(NULL);
    return NULL;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "usage:./webserver 8000\n");
        return -1;
    }
    unsigned short port = atoi(argv[1]);

    // create listen socket
    int lfd = socket(AF_INET, SOCK_STREAM, 0);
    if (lfd < 0) {
        perror("socket");
        return -1;
    }

    // reuse port
    int opt = 1;
    setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    // bind fixed port and ip to listen socket
    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    int bind_ret = bind(lfd, (const struct sockaddr *)&addr, sizeof(addr));
    if (bind_ret < 0) {
        perror("bind");
        return -1;
    }

    // listen
    int listen_ret = listen(lfd, 128);
    if (listen_ret < 0) {
        perror("listen");
        return -1;
    }

    while (1) {
        struct sockaddr_in cli_addr;
        bzero(&cli_addr, sizeof(cli_addr));

        socklen_t cli_len = sizeof(cli_addr);
        int cfd = accept(lfd, (struct sockaddr *)&cli_addr, &cli_len);
        if (cfd < 0) {
            perror("accept");
            break;
        } else {
            ClientMsg *p = calloc(1, sizeof(ClientMsg));
            p->cfd = cfd;
            p->addr = cli_addr;

            pthread_t tid;
            pthread_create(&tid, NULL, deal_client_fun, (void *)p);
            pthread_detach(tid);
        }
    }

    close(lfd);
    return 0;
}